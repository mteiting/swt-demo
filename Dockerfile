FROM mcr.microsoft.com/dotnet/core/sdk:2.2-alpine AS builder
WORKDIR /app

# copy files
COPY swt-demo/*.csproj .
RUN dotnet restore

COPY swt-demo/. .
RUN dotnet publish -o out -c Release

FROM mcr.microsoft.com/dotnet/core/runtime:2.2-alpine as runtime

WORKDIR /app
COPY --from=builder /app/out ./
ENTRYPOINT ["dotnet", "swt-demo.dll"]
